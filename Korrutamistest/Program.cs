﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Korrutamistest
{
    class Program
    {
        static void Main(string[] args)
        {
            // muutujad, mida mul on tööks vaja
            int punkte = 0; // palju õigeid vastuseid
            int kordi = 0;  // palju katseid
            Random r = new Random();  // juhuslikugene
            string[] hinnangud =    // erinevad vastused valele vastusele
            {
                    "mine proovi uuesti",
                    "õpi arvutama",
                    "kummipea oled",
                    "Lauajalg oskab ka paremini"
                };
            while (true)  // teeme niikaua, kui ta tahab
            {
                kordi++;     // katsete arv
                int mitmes = 0;   // mitmes vastusevariant (alguses nulliks)
                int korrutis = -1;   // et oleks millega võrrelda
                int yks = r.Next() % 100;   // kaks arvu, mida korrutada
                int kaks = r.Next() % 100;
                // anname teada
                Console.WriteLine($"mul on arvud {yks} ja {kaks}, korruta need!");
                do
                {
                    Console.Write("Vastus: ");
                    if (int.TryParse(Console.ReadLine(), out korrutis))
                    {
                        // kui oli vale vastus
                        if (korrutis != yks * kaks)
                        {
                            // anname oma arvamuse ja suurendame loendit
                            Console.WriteLine(hinnangud[mitmes++]);
                        }
                        else punkte++; // õige vastus annab punkti
                    }
                    // see, kui vastus on loll
                    else Console.WriteLine("õpi vastama");
                } while (korrutis != yks * kaks && mitmes <= hinnangud.Length );
                // lõpetame, kui katsed otsas või õige vastus
                Console.Write("nonii - kas jätkame (jah): ");
                if (Console.ReadLine() != "jah") break; // kui rohkem ei taha, siis lõpetame
            }
            // ja tulemus
            Console.WriteLine($"said tulemuseks {punkte} {kordi}-st");
        }
    }
}
